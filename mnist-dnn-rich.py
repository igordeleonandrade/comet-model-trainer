from __future__ import print_function
import logging
import argparse
import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

from comet_ml import Experiment

import tensorflow as tf
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten, Dense, Dropout

import postprocess


def build_model_graph(
    dropout: float = 0.2,
    optimizer: str = "adam",
    layer_1_size: int = 128,
    layer_2_size: int = 128,
    initial_lr: float = 1e-2,
    decay_steps: int = 2000,
    decay_rate: float = 0.9,
    input_shape: tuple = (784,),
    num_classes: int = 10,
    **kwargs
):
    model = Sequential(
        [
            Flatten(input_shape=input_shape),
            Dense(layer_1_size, activation="relu"),
            Dense(layer_2_size, activation="relu"),
            Dropout(dropout),
            Dense(num_classes),
        ]
    )

    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
        initial_learning_rate=initial_lr,
        decay_steps=decay_steps,
        decay_rate=decay_rate,
    )

    loss_fn = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
    model.compile(optimizer=optimizer, loss=loss_fn, metrics=["accuracy"])

    return model


def load_data():
    mnist = tf.keras.datasets.mnist
    num_classes = 10

    # the data, shuffled and split between train and test sets
    (x_train, y_train), (x_test, y_test) = mnist.load_data()

    x_train = x_train.reshape(60000, 784)
    x_test = x_test.reshape(10000, 784)

    x_train = x_train.astype("float32")
    x_test = x_test.astype("float32")

    x_train /= 255
    x_test /= 255

    # convert class vectors to binary class matrices
    y_train = to_categorical(y_train, num_classes)
    y_test = to_categorical(y_test, num_classes)

    return (x_train, y_train), (x_test, y_test)


def finalize_model(model, x_test, y_test, experiment, model_name="mnist-neural-net"):
    def test_index_to_example(index):
        img = x_test[index].reshape(28, 28)
        # log the data to Comet, whether it's log_image, log_text, log_audio, ...
        data = experiment.log_image(img, name="test_%d.png" % index)

        if data is None:
            return None

        return {"sample": str(index), "assetId": data["imageId"]}

    # Add tags
    experiment.add_tag("mnist")
    experiment.add_tag("keras")

    # Confusion Matrix
    preds = model.predict(x_test)
    experiment.log_confusion_matrix(
        y_test, preds, index_to_example_function=test_index_to_example
    )

    # Log Model
    save_path = "./models"
    os.makedirs(save_path, exist_ok=True)

    model_path = os.path.join(save_path, "mnist-nn.h5")
    model.save(model_path)
    experiment.log_model(model_name, model_path)


def train(params):
    # Autologging Weights, Gradients and Activation is currently
    # only supported with Keras
    experiment = Experiment(
        auto_histogram_weight_logging=True,
        auto_histogram_gradient_logging=True,
        auto_histogram_activation_logging=True,
    )

    # log custom hyperparameters
    experiment.log_parameters(params)

    (x_train, y_train), (x_test, y_test) = load_data()

    print(x_train.shape[0], "train samples")
    print(x_test.shape[0], "test samples")

    batch_size = params["batch_size"]
    epochs = params["epochs"]

    # Define model
    model = build_model_graph(**params)
    model.fit(
        x_train,
        y_train,
        batch_size=batch_size,
        epochs=epochs,
        validation_data=(x_test, y_test),
    )

    score = model.evaluate(x_test, y_test, verbose=0)
    logging.info("Score %s", score)

    finalize_model(model, x_test, y_test, experiment, model_name=params["model_name"])
    postprocess.create_mr_artifacts(
        "./artifacts",
        os.getenv("COMET_WORKSPACE"),
        os.getenv("COMET_PROJECT_NAME"),
        experiment.id,
    )


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--epochs", type=int, default=10)
    parser.add_argument("--batch_size", type=int, default=64)
    parser.add_argument("--dropout", type=float, default=0.2)
    parser.add_argument("--layer_1_size", type=int, default=128)
    parser.add_argument("--layer_2_size", type=int, default=128)
    parser.add_argument("--initial_lr", type=float, default=1.0e-2)
    parser.add_argument("--decay_steps", type=int, default=2000)
    parser.add_argument("--decay_rate", type=float, default=0.9)
    parser.add_argument("--optimizer", type=str, default="adam")
    parser.add_argument("--model_name", type=str, default="mnist-neural-net")

    return parser.parse_args()


def main():
    args = get_args()

    params = {
        "epochs": args.epochs,
        "batch_size": args.batch_size,
        "dropout": args.dropout,
        "layer_1_size": args.layer_1_size,
        "layer_2_size": args.layer_2_size,
        "initial_lr": args.initial_lr,
        "decay_steps": args.decay_steps,
        "decay_rate": args.decay_rate,
        "optimizer": args.optimizer,
        "model_name": args.model_name,
    }
    train(params)


if __name__ == "__main__":
    main()
